#! /usr/bin/perl -w

# Based on koffie.pl
# Made into lunch.pl by `36`

use strict;

my %list;

sub loadlist {
	open (LIST, "<lunch.list") or return;

	while (my $line = <LIST>) {
		chomp $line;
		my $i = 1;
		my @lineparts = split ",", $line;
		$i = $lineparts[1] unless @lineparts < 2;
		$list{$lineparts[0]} = $i;
	}

	close LIST;
}

my %syns = (
	'1' => 1,
	'0' => 0,
	'vegetarian' => 6,
	'vegan' => 13,
);

sub savelist {
	open (LIST, ">lunch.list") or die "Can't open lunch.list for writing";
	
	foreach my $key (sort keys %list) {
		print LIST "$key,$list{$key}\n";
	}
	close LIST;
}

sub loadsyns {
	open (SYNS, "<syns.list") or open (SYNS, "<syns.default") or die "Can't open syns.list nor syns.default for reading";

	# 1
	{
		my $line = <SYNS> | '';
		chomp $line;
		my @options = split ",", $line;

		foreach my $option (@options) {
			if ($option =~ /^\w+$/ and not exists $syns{$option}) {
				$syns{$option} = 1;
			}
		}
	}

	# 0
	{
		my $line = <SYNS> | '';
		chomp $line;
		my @options = split ",", $line;

		foreach my $option (@options) {
			if ($option =~ /^\w+$/ and not exists $syns{$option}) {
				$syns{$option} = 0;
			}
		}
	}
	
	# vegetarian / 6
	{
		my $line = <SYNS> | '';
		chomp $line;
		my @options = split ",", $line;

		foreach my $option (@options) {
			if ($option =~ /^\w+$/ and not exists $syns{$option}) {
				$syns{$option} = 6;
			}
		}
	}
	
	# vegan / 13
	{
		my $line = <SYNS> | '';
		chomp $line;
		my @options = split ",", $line;

		foreach my $option (@options) {
			if ($option =~ /^\w+$/ and not exists $syns{$option}) {
				$syns{$option} = 13;
			}
		}
	}

	close SYNS;
}

sub printsyns {
	my ($fh) = @_;
	
	my @true;
	my @false;
	my @vegetarian;
	my @vegan;

	foreach my $key (sort keys %syns) {
		push @true, $key if ($syns{$key} == 1);
		push @false, $key if ($syns{$key} == 0);
		push @vegetarian, $key if ($syns{$key} == 6);
		push @vegan, $key if ($syns{$key} == 13);
	}

	my $trueline = join ',', @true;
	my $falseline = join ',', @false;
	my $vegetarianline = join ',', @vegetarian;
	my $veganline = join ',', @vegan;

	print $fh "$trueline\n";
	print $fh "$falseline\n";
	print $fh "$vegetarianline\n";
	print $fh "$veganline\n";
}

sub savesyns {
	open (SYNS, ">syns.list") or die "Can't open syns.list for writing";

	printsyns(\*SYNS);

	close SYNS;
}

my $multi_user = $ENV{MULTI_USER};
$multi_user =~ s/^.*!(.*)!.*$/$1/ if defined $multi_user;

my $nick = (defined $ENV{MULTI_REALUSER} && $ENV{MULTI_REALUSER} eq
	'pietjepuk') ? $multi_user : $ENV{MULTI_REALUSER};

die "\$nick undefined!\n" unless defined $nick;

my @args = (defined $ARGV[0] ? (split /\s+/, $ARGV[0]) : ());

loadsyns();

if (@args == 1 and $args[0] =~ m/^(reset|clear)$/i ) {
	loadlist();
	%list = ();
	savelist();
	print "list cleared\n";
}
elsif (@args >= 2 and $args[0] eq 'syn') {
	if (@args == 2 and $args[1] eq 'list') {
		printsyns(\*STDOUT);
	}
	elsif (@args == 4 and $args[1] eq 'add') {
		if ($ENV{'MULTI_USERLEVEL'} > 99) {
			if (exists $syns{$args[3]} and not exists $syns{$args[2]}) {
				$syns{$args[2]} = $syns{$args[3]};
				print "Added: " . $args[2] . " -> " . $args[3] . "\n";
				savesyns();
			}
			elsif (exists $syns{$args[2]} and not exists $syns{$args[3]}) {
				$syns{$args[3]} = $syns{$args[2]};
				print "Added: " . $args[3] . " -> " . $args[2] . "\n";
				savesyns();
			}
			else {
				# When both $syns{$args[2]} and $syns{$args[3]} do or do not exist.
				print "No, just no!\n";
			}
		}
		else {
			# When insufficient level.
			print "Nope, not going to do that for you.\n";
		}
	}
	else {
		print "Error: please consult !help lunch\n";
	}
}
elsif (@args == 0 or (@args == 1 and $args[0] =~ /^(all|list|lijst)$/io ) ) {
	loadlist();
	if (%list) {
		my $persons = '';
		my $count_other = 0;
		foreach my $key (sort keys %list) {
			if ($list{$key} == 1) {
				$persons .= $key .", ";
			}
			else {
				$persons .= "\cC$list{$key}$key\cC, ";
				$count_other++;
			}
		}
		$persons =~ s/, $//;
		my $count = scalar keys %list;
		if ($count_other > 0) {
			print "$persons [count: $count of which $count_other special]\n";
		} else {
			print "$persons [count: $count]\n";
		}
	}
	else {
		print "[count: 0]\n";
	}
}
elsif (@args >= 1 and $args[0] =~ /^(bootschappen|extra)$/io ) {
	# TODO: implement
}
elsif (@args == 1 and exists $syns{$args[0]} and $syns{$args[0]} == 1) {
	loadlist();
	$list{$nick} = 1;
	savelist();
	print "$nick put on list\n";
}
elsif (@args == 1 and exists $syns{$args[0]} and $syns{$args[0]} == 0) {
	loadlist();
	if (exists $list{$nick}) {
		delete($list{$nick});
		savelist();
		print "$nick removed from list\n";
	}
	else {
		print "$nick not found in list\n";
	}
}
elsif (@args == 1 and exists $syns{$args[0]} and $syns{$args[0]} == 6) {
	loadlist();
	$list{$nick} = 13;
	savelist();
	print "$nick put on list as vegetarian\n";
}
elsif (@args == 1 and exists $syns{$args[0]} and $syns{$args[0]} == 13) {
	loadlist();
	$list{$nick} = 13;
	savelist();
	print "$nick put on list as vegan\n";
}
else {
	print "Error: please consult !help lunch\n";
}
